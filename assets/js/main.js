$(function () {
    // WOW
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();

    // SCROLL TOP
    $('.app-go-on-top').on('click', function (event) {
        event.preventDefault();
        console.log('click')
        $('html, body').animate({
            scrollTop: 0
        }, 1000)
    })

    $(window).scroll(function () {
        var n = $('html, body').scrollTop();
        if (n >= 300) {
            $('.menu-bar').addClass('fixxed');
        }
        else {
            $('.menu-bar').removeClass('fixxed');
        }
    });

    // NAVBAR HEADER CLICK
    $('.navbar-header').click(function (e) {
        // e.preventDefault();
        $('.navbar-header.active').removeClass('active');
        $(this).addClass('active')
    });

    // SHOW MENU
    $('.menu-logo-slide').slideUp();
    $('.menu__logo-bar-function i:nth-child(3)').click(function (e) {
        e.preventDefault();
        $('.menu-logo-slide').slideToggle();
    });


    // SHOW MENU CHILD
    $('.menu-logo-item--link').slideUp();
    $('.menu-logo-slide ul li a i').click(function (e) {
        e.preventDefault();
        // console.log($(this))
        $(this).parents().next('.menu-logo-item--link').slideToggle();
        // $('.menu-logo-item--link').slideToggle();
    });

    // SHOW SEARCH BOX
    $('.menu__logo-bar-function i:nth-child(1)').click(function (e) {
        e.preventDefault();
        $('.search-box').toggleClass('active');
        console.log('click')
    });
    // SHOW SEARCH BOX
    $('.menu__logo-bar-function i:nth-child(2)').click(function (e) {
        e.preventDefault();
        $('.cart-box').toggleClass('active');
        console.log('click')
    });

    // SHOW SEARCH ON PC
    $('.navbar-header-icon').click(function (e) {
        e.preventDefault();
        $(this).find('.navbar-header__input').toggleClass('active');
        $(this).find('.navbar-header-icon i').toggleClass('active');
    });

    // SHOW PRODUCT MODE
    $('.shop-view-btn').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            // $(this).removeClass('active')
        }
        else {
            $('.shop-view-btn.active').removeClass('active')
            $(this).addClass('active')
        }
        var views = 0;
        if ($('.shop-view-btn__grid').hasClass('active')) {
            views = 1;
            console.log(views)
            $('.shop-item').removeClass('col-xs-12 col-sm-12 col-md-6 col-lg-12 shop-item-large');
            $('.shop-item').addClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');
        }
        if ($('.shop-view-btn__list').hasClass('active')) {
            views = 0;
            console.log(views)
            $('.shop-item').removeClass('col-xs-12 col-sm-4 col-md-4 col-lg-4');
            $('.shop-item').addClass('col-xs-12 col-sm-12 col-md-6 col-lg-12 shop-item-large');
        }
    });

    // FAQ SLIDERS

    $('.faq-content__item p').slideUp();
    $('.faq-content__item:nth-child(1) p').slideDown();

    $('.faq-content__item a').click(function (e) {
        e.preventDefault();
        console.log(123)
        $('.faq-content__item p').slideUp();
        $(this).next().slideToggle();
    });


    //
    $('.fa-minus').addClass('active');
    var a = 0;
    $('.collapse-control').click(function (e) {
        e.preventDefault();
        $('.fa-plus').addClass('active');
        $('.fa-minus').removeClass('active');
        if (!$(this).hasClass('collapsed')) {
            $(this).find('.fa-plus').addClass("active");
            $(this).find('.fa-minus').removeClass('active');
            console.log($(this).find('.fa-plus'));
        }
        else {
            $(this).find('.fa-minus').addClass('active');
            $(this).find('.fa-plus').removeClass('active');
        }
    });







    // SLIDER
    $('#slider .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        items: 1,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 1000,
    });

    // HANDLE REGISTER FORM
    $('#btn-login').click(function (e) {
        e.preventDefault();
        $('.register-form').removeClass('active');
        $('.login-form').addClass('active');
    });

    // HANDLE REGISTER FORM
    $('#btn-register').click(function (e) {
        e.preventDefault();
        $('.login-form').removeClass('active');
        $('.register-form').addClass('active');
    });

    // TABS LIST    
    $('.offers__btn-item').click(function (e) {
        e.preventDefault();
        $('.offers__btn-item.active').removeClass('active');
        $(this).addClass('active')
    });

    var slider = document.getElementById("range-slide");
    var output = document.getElementById("product-price");
    /* Hiển thị giá trị mặc định 50 cho thẻ span với id là demo*/
    output.innerHTML = slider.value;
    /* cập nhật giá trị hiện tại của ranger slider*/
    slider.oninput = function () {
        output.innerHTML = slider.value;
    };

    // FILTER
    $("#filter_btn").click(function (e) {
        e.preventDefault();
    });


});